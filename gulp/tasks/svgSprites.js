import svgSprive from "gulp-svg-sprite";
export const svgSprites = (done)=> {
    app.plugins.if(
        app.isBuild,app.gulp.src(app.path.src.svgIcons, {sourcemaps: true}))
        .pipe(app.plugins.if(
            app.isBuild,app.gulp.dest(`${app.path.build.images}/icons`)))
    app.gulp.src(app.path.src.svgIcons, {sourcemaps: true})
        .pipe(app.plugins.plumber(app.plugins.notify.onError({
            title: "SVG",
            massage: "Error: <%= error.message %>"
        })))
        .pipe(svgSprive({
            mode: {
                stack: {
                    sprite: `../icons/svg-sprite.html`,
                    example:false,
                }
            }
            })
        )
        .pipe(app.gulp.dest(app.path.build.svgSprites));
    done();
}
