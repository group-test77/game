import concatJs from "gulp-concat";
import uglifyJs from "gulp-uglify";
export const js = (done)=> {
app.gulp.src(app.path.src.js, {sourcemaps: app.isDev})
    .pipe(app.plugins.plumber(app.plugins.notify.onError({
        title: "JS",
        massage: "Error: <%= error.message %>"
    })))
    .pipe(concatJs("app.min.js"))
    .pipe(app.plugins.if(
        app.isBuild,uglifyJs()))
    .pipe(app.gulp.dest(app.path.build.js))
    .pipe(app.plugins.browserSync.stream());
    done();
}