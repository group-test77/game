class Game {
    constructor(levelDifficulty, tr, td) {
       this._levelDifficulty = levelDifficulty;
       this._tr = tr;
       this._td = td;
    }
    set levelDifficulty (value){
    }
    get levelDifficulty(){
        return this._levelDifficulty;
    }
    set tr (value){
    }
    get tr(){
        return this._tr;
    }
    set td (value){
    }
    get td (){
        return this._td;
    }
    gameStart(){
        this.iterationCount = 0;
        const count = {
            computer : 0,
            user: 0,
            general: 0,
        }
        let delay;
        if (this.levelDifficulty ==="level-easy"){
            delay = 1.9;
        }
        if (this.levelDifficulty ==="level-medium"){
            delay = 0.8;
        }
        if (this.levelDifficulty ==="level-hard"){
            delay = 0.5;
        }
        this.arrayCell = [];
        for (let x = 0; x<=99; x++){
            this.arrayCell.push(`cell-${x}`)
        }
        let currentClick;
        document.addEventListener("click", (e)=>{
            if (e.target.closest("td")){
                currentClick = e.target;
                if (currentClick===this.currentCell){
                    this.targetBackground = this.currentCell.style.backgroundColor = "#00FF00";
                }
            }
        })
         const game =()=> {
            if (count.general >= 51){
                new General().renderFinish(count);
                return;
            }
           let randomNumber = this.randomizer();
             while (!this.arrayCell[randomNumber]){
                 randomNumber = this.randomizer();
             }
           this.currentCell = this.arrayCell[randomNumber];
             this.currentCell = document.querySelector(`.${this.currentCell}`);
             this.targetBackground = this.currentCell.style.backgroundColor = "#0000ff";
                 setTimeout(()=>{
                     if (this.targetBackground === "#00FF00"){
                         count.user++;
                     }
                     else {
                         this.currentCell.style.backgroundColor = "#cc0000";
                         count.computer++
                     }

                     delete this.arrayCell[randomNumber];
                     this.iterationCount =1;
                     currentClick = 0;
                     count.general++;
                     game();
                 }, delay*1000)
        }
        game();
    }
    randomizer(){
        this.number = Math.trunc((Math.random()*100));
        if (this.iterationCount === 0){
            return this.number;
        }
        else {
                return this.number;
        }
    }
}
