class General {
    render =()=>{
        this.levelChose = document.querySelectorAll("input");
        for (const levelChoseElement of this.levelChose) {
            if (levelChoseElement.checked){
                this.levelChose = levelChoseElement.id;
            }
        }
        if (typeof this.levelChose !== "string"){
            return;
        }
        this.delForm();
this.renderGame();
     }
     delForm(){
         const formWrapper = document.querySelector(".content-wrapper");
         formWrapper.style.display = "none";
     }
     renderGame(){
       const tableRoot = document.querySelector("#root");
          tableRoot.style.display = "grid";
         this.classAdd();
this.countdown();
     }
     classAdd(){
         this.td = document.querySelectorAll("td");
         let counter = 0;
         for (const element of this.td) {
             element.classList.add(`cell-${counter}`);
             counter++;
             if (counter===100){
                 break
             }
         }
     }
     countdown(){
        const counter = document.querySelector(".counter");
        let counterTime = counter.textContent;
        const timer = setInterval(()=>{
            counterTime--;
            counter.textContent = counterTime;
            if (counterTime===0){
                counter.style.display = "none";
                clearInterval(timer);
                new Game(this.levelChose, this.tr, this.td).gameStart();
            }
        },1000);
     }
     renderFinish (count){
        const root = document.querySelector("#root");
        const btnMenu = document.querySelector(".btn-menu");
const scoreWrapper = document.querySelector(".score");
const winner = document.querySelector(".winner");
         const userScore = document.querySelector(".user-score");
         const computerScore = document.querySelector(".computer-score");
         if (count.user>count.computer){
             winner.textContent = "You are win!";
         }
         else {
             winner.textContent = "You are lose";
         }
         userScore.textContent = `Your score: ${count.user}`;
         computerScore.textContent = `Computer score: ${count.computer}`;
         scoreWrapper.style.display = "block";
btnMenu.style.display = "block";
         root.style.alignContent = "normal";
     }
}